﻿using System;

namespace CloneAndPrtotype
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student1 = new Student() { group = new Group { Course = 1 } };

            Student student2 =(Student)student1.Clone();

            student2.group.Course = 2;

            Console.WriteLine("Курс {0}", student1.group.Course);
            Console.WriteLine("Курс {0}", student2.group.Course);

            Console.ReadLine();
        }
    }


    public interface IClonable
    {
        object Clone();
    }

    public class Group
    {
        public int Course;
        public string Spec;
    }


    public class Student : IClonable
    {
        public string name, surname;

        public int age;

        public Group group;


        public object Clone()
        {
            Group newGroup = new Group { Course = this.group.Course, Spec = this.group.Spec };
            return new Student { name = this.name, surname = this.surname, age = this.age, group = newGroup};
        }
 
    }
}
